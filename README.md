The rt-stepper project provides a software/hardware solution for a USB CNC controller. The PC side application is based on the popular EMC2 software at www.linuxcnc.org. 
By re-purposing EMC2 software the rt-stepper solution provides a complete gcode interpreter, a trajectory planner, a GUI front-end, backplotting, spindle speed and more. 
Since the rt-stepper hardware provides the real time step pulses, you can now drive your CNC controller from standard PCs running Linux, Mac OSX, and Windows.

For more details go to www.ecklersoft.com

